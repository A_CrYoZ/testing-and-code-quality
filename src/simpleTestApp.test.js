const startServer = require("./simpleTestApp");
const supertest = require("supertest");

let app;

beforeAll(async () => {
	app = await startServer();
});

afterAll(() => app.stop());

describe("simpleTestApp", function () {
	it("Returns empty list of tasks on get request", async () => {
		const response = await supertest(app).get("/");
		expect(response.body).toEqual([]);
	});

	it("Returns 404 on incorrect path", async () => {
		await supertest(app).get("/somePath").expect(404);
	});

	it("should return 400 status code if title is incorrect", async () => {
		const res = await supertest(app).post("/").send({
			title: "12",
		});

		expect(res.statusCode).toEqual(400);
		expect(res.body.errors[0].msg).toStrictEqual(
			"Minimal length for task name is 3 letter!"
		);
	});

	it("should return 400 status code if description is incorrect", async () => {
		const res = await supertest(app).post("/").send({
			description: 404,
		});

		expect(res.statusCode).toEqual(400);
		expect(res.body.errors[0].msg).toStrictEqual("Invalid value");
	});

	it("Should create a task", async () => {
		await supertest(app)
			.post("/")
			.send({
				title: "Jest title",
				description: "Jest description",
			})
			.expect(200);
	});

	it('should return array of 1 element with task title "Jest title"', async () => {
		const res = await supertest(app).get("/");

		expect(res.statusCode).toEqual(200);
		expect(res.body.length).toStrictEqual(1);
		expect(res.body[0].title).toStrictEqual("Jest title");
	});
});
