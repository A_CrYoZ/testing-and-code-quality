jest.mock("./getFileContent");

const getFileContent = require("./getFileContent");
const findText = require("./findText");

describe("findText", function () {
	it('should return true if text "testString" was found', async () => {
		const expectedString = "testString";
		const fPath = "./somefile.txt";

		getFileContent.mockResolvedValue(expectedString);

		const res = await findText(fPath, expectedString);
		expect(res).toBeTruthy();
	});

	it('should return false if text "testString" was not found', async () => {
		const expectedString = "testString";
		const resolvedString = "Lorem Ipsum";
		const fPath = "./somefile.txt";

		getFileContent.mockResolvedValue(resolvedString);

		const res = await findText(fPath, expectedString);
		expect(res).toBeFalsy();
	});

	it("should return error if the incorrect path provided", async () => {
		const expectedString = "testString";
		const fPath = "./somefile.txt";

		getFileContent.mockImplementation(() => {
			throw Error("Incorrect path");
		});

		await expect(() => findText(fPath, expectedString)).rejects.toThrow(
			"Incorrect path"
		);
	});
});
