const getFibonacci = require("./fibonacci");

describe("fibonacci", function () {
	it("should be 34 for given input (9)", () => {
		expect(getFibonacci(9)).toStrictEqual(34);
	});

	it("should be 1 for given input (1)", () => {
		expect(getFibonacci(1)).toStrictEqual(1);
	});

	it("should be 0 for given input (-1)", () => {
		expect(getFibonacci(-1)).toStrictEqual(0);
	});

	it("should be RangeError for given input (Number.MAX)", async () => {
		expect(() => getFibonacci(Number.MAX_VALUE)).toThrow(RangeError);
	});

	it('should be RangeError for given input ("abcd")', () => {
		expect(() => {
			getFibonacci("abcd");
		}).toThrow(RangeError);
	});
});
